import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { pizzalist } from './pizzalist.js';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
})
export class OrderComponent implements OnInit {
  public myForm: FormGroup;
  public name: string;
  public surName: string;
  public email: string;
  public pizzanames: any = [];
  public totalAmount: number;

  constructor() {
    this.pizzanames = pizzalist;
  }
  extras = [
    { name: 'Extra Cheese', price: 1.2 },
    { name: 'Extra veggies', price: 1.8 },
    { name: 'Ham', price: 1.5 },
    { name: 'Onions ', price: 0.7 },
    { name: 'Garlic ', price: 0.8 },
    { name: 'Tuna ', price: 2.2 },
  ];
  sizes = [
    { size: 28, price: 8.1 },
    { size: 30, price: 9.2 },
    { size: 50, price: 14.2 },
  ];

  ngOnInit(): void {
    this.myForm = new FormGroup({
      name: new FormControl('', Validators.required),
      surName: new FormControl(''),
      email: new FormControl('', [Validators.required, Validators.email]),
      address: new FormControl('', [
        Validators.required,
        Validators.pattern(/[a-z]+ [0-9]+/),
      ]),
      city: new FormControl('', Validators.required),
      toppings: new FormArray(this.createFormArray()),
      checkBox: new FormControl(),
      pizzaBase: new FormControl(this.pizzanames[0].price),
      pizzaSize: new FormControl(this.sizes[1].price),
    });
  }

  onSubmit(data: FormGroup): void {
    alert(
      `Please verify your details: Name: ${data.value.name},
      Surname: ${data.value.surName}, Email address: ${data.value.email},
      Address:${data.value.address}, City: ${data.value.city}`
    );
  }
  createFormArray(): FormControl[] {
    const controlls = [];
    for (let i = 0; i < this.extras.length; i++) {
      const controll = new FormControl();
      controlls.push(controll);
    }

    return controlls;
  }
  getExtrasControll(index): any {
    return (this.myForm.get('toppings') as FormArray).at(index);
  }
  calculatePrice() {
    let total: number[] = [];
    let sum: number = 0;
    console.log(this.myForm.value);
    for (let j = 0; j < this.myForm.value.toppings.length; j++) {
      if (this.myForm.value.toppings[j] === true) {
        console.log('price', this.extras[j].price);
        total.push(this.extras[j].price);
      }
    }
    total.push(this.myForm?.value?.pizzaBase);
    total.push(this.myForm?.value?.pizzaSize);
    console.log('prices', total);
    for (let i = 0; i < total.length; i++) {
      sum += total[i];
    }
    this.totalAmount = sum;
    console.log('sum', sum);
  }
}
