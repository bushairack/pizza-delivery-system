export const pizzalist = [
  {
    id: 1,
    name: "Pizza veggie",
    price: 5.99,
    description: "Fresh delicious and without meat",
  },
  {
    id: 2,
    name: "Pizza salami",
    price: 10.99,
    description: "Fresh delicious with bio salami",
  },
  {
    id: 3,
    name: "Pizza tuna",
    price: 7.99,
    description: "Fresh delicious with tasty tuna",
  },
  {
    id: 4,
    name: "Pizza four cheese",
    price: 8.99,
    description: "Fresh delicious with four different cheese types and garlic",
  },
  {
    id: 5,
    name: "Pizza pepper",
    price: 7.49,
    description: "Fresh delicious with spicy pepper",
  },
];
