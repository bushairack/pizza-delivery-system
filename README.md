## Pizza Order Application
This app is designed for ordering pizza. The following requirements are implemented using Angular (Use the reactive Forms module).
1. Create an app with the following input fields:
    * Name (text)
    * Surename (text)
    * Email Adress (email)
    * Submit button
    * Reset button

    The user should be able to fill out all fields. He/she should get the information displayed in an "alert()"-dialog when clicking on submit.
    
    The user can reset all form fields when he/she clicks on reset.
2. Create a radiobutton for pizza size (only one option should be selectable at a time)
    * 28cm (8,10€)
    * 30cm (9,20€) (default)
    *  50cm (family size) (14,20€)
3. Create a dropdown for selecting from pre build pizzas("Pizza veggie" is default)
    * pizzalist.js
4. Create a checkbox for each of the      following toppings:
    * extra cheese (1,20€)
    * extra veggies (1,80€)
    * ham (1,50€)
    * onions (0,70€)
    * garlic (0,80€)
    * tuna (2,20€)

Calculate the price for current selection and display it next to the submit button.

Choose a font size and nice layout that no scrollbar is visible in fullscreen.

Sample Output:

![alt text](pizzaAppScreenShot.png)

